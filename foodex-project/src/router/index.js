import Dashboard from '@/components/Dashboard';
import Admin from '@/components/Admin';
import ProductsList from '@/components/ProductsList';
import ProductsTrash from '@/components/ProductsTrash';
import ProductEdit from '@/components/ProductEdit';
import ShtoShtetin from '@/components/ShtoShtetin';
import EditoShtetin from '@/components/EditoShtetin';
import Messages from '@/components/Messages';
import Login from '@/components/auth/Login';
import Category from '@/components/Category.vue';
import ShtoMenaxherin from '@/components/ShtoMenaxherin.vue';
import ShtoPunetorin from '@/components/ShtoPunetorin.vue';
import ShtoPostierin from '@/components/ShtoPostierin.vue';
import CategoryEdit from '@/components/CategoryEdit.vue';
import Stafi from '@/components/Stafi.vue';
import Orders from '@/components/Orders';
import ConfirmedOrders from '@/components/ConfirmedOrders';
import Orderdetails from '@/components/Orderdetails';
import Deliver from '@/components/Deliver';
import ReadMessage from '@/components/ReadMessage';
import Vue from 'vue';
import Router from 'vue-router';
import firebase from 'firebase';
//importojme gjithe ato komponenta qe do i regjistrojme si routes
Vue.use(Router);

let router = new Router({
 routes : [
    
    { path: '/productslist', name:'productslist', component:ProductsList, meta: {requiresAuth: true}},
    { path: '/productedit/:Pid', name:'productedit', component:ProductEdit,meta: {requiresAuth: true} },
    { path: '/productstrash/', name:'productstrash', component:ProductsTrash,meta: {requiresAuth: true}},
    { path: '/shtoshtetin', name:'shtoshtetin', component:ShtoShtetin, meta: {requiresAuth: true}},
    { path: '/editoshtetin/:Sid', name:'editoshtetin', component:EditoShtetin, meta: {requiresAuth: true}},
    { path: '/messages', name:'messages', component:Messages, meta: {requiresAuth: true}},
    { path: '/', name:'admin', component:Admin, meta: {requiresAuth: true}},
    { path: '/category', name:'category', component:Category, meta: {requiresAuth: true}},
    { path: '/categoryedit/:Cid', name:'categoryedit', component:CategoryEdit, meta: {requiresAuth: true}},
    { path: '/dashboard', name:'dashboard', component:Dashboard, meta: {requiresAuth: true}},
    { path: '/login', name:'login', component:Login, meta: {requiresGuest: true}},
    { path: '/shtomenaxherin', name:'shtomenaxherin', component:ShtoMenaxherin, meta: {requiresAuth: true}},   
    { path: '/shtopunetorin', name:'shtopunetorin', component:ShtoPunetorin, meta: {requiresAuth: true}},
    { path: '/shtopostierin', name:'shtopostierin', component:ShtoPostierin, meta: {requiresAuth: true}},    
    { path: '/stafi', name:'stafi', component:Stafi, meta: {requiresAuth: true}},
    { path: '/orders', name:'orders', component:Orders, meta: {requiresAuth: true}},
    { path: '/orderdetails/:Oid', name:'orderdetails', component:Orderdetails, meta: {requiresAuth: true}},
    { path: '/confirmedorders', name:'confirmedorders', component:ConfirmedOrders, meta: {requiresAuth: true}},
    { path: '/deliver', name:'deliver', component:Deliver, meta: {requiresAuth: true}},
    { path: '/readmessage/:Tid', name:'readmessage', component:ReadMessage, meta: {requiresAuth: true}}
  ]

});

router.beforeEach((to, from, next) => {
    // Check for requiresAuth guard
    if (to.matched.some(record => record.meta.requiresAuth)) {
      // Check if NO logged user
      if (!firebase.auth().currentUser) {
        // Go to login
        next({
          path: '/login',
          
        });
      } else {
        // Proceed to route
        next();
      }
    } else if (to.matched.some(record => record.meta.requiresGuest)) {
      // Check if NO logged user
      if (firebase.auth().currentUser) {
        // Go to login
        next({
          path: '/',
        
        });
      } else {
        // Proceed to route
        next();
      }
    } else {
      // Proceed to route
      next();
    }
  });
  
  export default router;
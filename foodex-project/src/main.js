import Vue from 'vue'
import App from './App.vue'
import firebase from 'firebase'
import VueRouter from 'vue-router'
import Chartkick from 'vue-chartkick'
import Vuelidate from 'vuelidate';
import Chart from 'chart.js';
import VModal from 'vue-js-modal';
import VueGoogleCharts from 'vue-google-charts';
//fajlli routes importohet te kjo pjese dhe ne at file kemi deklaruar routat e ndryshem se cila route te cila komponent dergon gjate klikut

import router from './router';
//vue router e instalojme me npm install vue-router pastaj e thirrim ne main.js me import VueRouter from 'vue-router'
Vue.use(Vuelidate)
import store from "./store";
//regjistrimi i filterit global per zvogelimin e shkronjave
Vue.filter('to-lowercase',function(value){
  return value.toLowerCase();
});



const configOptions = {
  apiKey: "AIzaSyBGRvGq4zosW6EVdXMxQX-767FV4G3jNmw",
  authDomain: "libraria-89131.firebaseapp.com",
  databaseURL: "https://libraria-89131.firebaseio.com",
  projectId: "libraria-89131",
  storageBucket: "libraria-89131.appspot.com",
  messagingSenderId: "364752324957",
  appId: "1:364752324957:web:557d8b05545d3a6a1a40b4"
};

firebase.initializeApp(configOptions);



Vue.use(VueRouter);
Vue.component("vue-modal", VModal);
Vue.use(Chartkick.use(Chart));
Vue.use(VueGoogleCharts);
Vue.use(Chartkick)

let app;
firebase.auth().onAuthStateChanged(function(user){
  store.dispatch("fetchUser", user);
  if(!app){
  app = new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
  });
}
});